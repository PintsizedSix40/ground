package com.PintsizedSix40.plugin;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.KeyPressedEvent;
import de.paxii.clarinet.event.events.player.PlayerMoveEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

public class ModuleGround extends Module{

	
	private double cury;
	private double inc;
	private boolean jump;
	public ModuleGround() {
		super("Ground", ModuleCategory.MOVEMENT);
		
		this.setVersion("1.0");
		this.setBuildVersion(100);
		this.setDescription("Forces you to be unable to fall at your current y-level. Probably counts as fly.");
		this.setRegistered(true);
		inc = .00000;
		jump = false;
	}
	
	@Override
	public void onEnable() {
		cury = Wrapper.getPlayer().posY;
		this.sendClientMessage("Set Y to "+cury);
	}
	
	 @EventHandler
	  public void onKeyPressed(KeyPressedEvent e) {
		if(Wrapper.getMinecraft().gameSettings.keyBindJump.getKeyCode() == e.getKey()) {
			jump = true;
			Wrapper.getMinecraft().player.jump();
		}
	  }
	  
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		if(e.getPosY() < cury && !jump) {
			e.setPosY(Math.ceil(e.getPosY()));
			e.setMotionY(0);
		}
	}
	  
}
